using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Zahlenvergleich
{
    public partial class Zahlenvergleich : Form
    {
        public Zahlenvergleich()
        {
            InitializeComponent();
            ToolTip tp = new ToolTip();
            tp.SetToolTip(cmd_clear, "Eingabe und Anzeigefelder löschen");
            tp.SetToolTip(cmd_end, "Programm beenden");
            tp.SetToolTip(cmd_show, "Zahlen vergleichen");
            tp.SetToolTip(txt_zahl1, "Zahl 1");
            tp.SetToolTip(txt_zahl2, "Zahl 2");
        }

        private void cmd_end_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void cmd_clear_Click(object sender, EventArgs e)
        {
            txt_zahl1.Text = txt_zahl2.Text = lbl_output.Text = null;
            txt_zahl1.Focus();
        }

        private void cmd_show_Click(object sender, EventArgs e)
        {
            // Variablen
            double zahl1, zahl2;
            try
            {
                zahl1 = Convert.ToDouble(txt_zahl1.Text);
                zahl2 = Convert.ToDouble(txt_zahl2.Text);
                if (zahl1 < zahl2)
                    lbl_output.Text = "Zahl 1 ist kleiner Zahl 2.";
                else
                    if (zahl1 > zahl2)
                        lbl_output.Text = "Zahl 1 ist größer Zahl 2.";
                    else
                        lbl_output.Text = "Beide Zahlen sind gleich.";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                txt_zahl1.Focus();
            }
        }
    }
}