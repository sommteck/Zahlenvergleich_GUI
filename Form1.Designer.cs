namespace Zahlenvergleich
{
    partial class Zahlenvergleich
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbl_output = new System.Windows.Forms.Label();
            this.txt_zahl1 = new System.Windows.Forms.TextBox();
            this.txt_zahl2 = new System.Windows.Forms.TextBox();
            this.cmd_show = new System.Windows.Forms.Button();
            this.cmd_clear = new System.Windows.Forms.Button();
            this.cmd_end = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Zahl 1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(32, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Zahl 2";
            // 
            // lbl_output
            // 
            this.lbl_output.AutoSize = true;
            this.lbl_output.Location = new System.Drawing.Point(35, 119);
            this.lbl_output.Name = "lbl_output";
            this.lbl_output.Size = new System.Drawing.Size(0, 16);
            this.lbl_output.TabIndex = 2;
            // 
            // txt_zahl1
            // 
            this.txt_zahl1.Location = new System.Drawing.Point(158, 38);
            this.txt_zahl1.Name = "txt_zahl1";
            this.txt_zahl1.Size = new System.Drawing.Size(100, 22);
            this.txt_zahl1.TabIndex = 1;
            // 
            // txt_zahl2
            // 
            this.txt_zahl2.Location = new System.Drawing.Point(158, 76);
            this.txt_zahl2.Name = "txt_zahl2";
            this.txt_zahl2.Size = new System.Drawing.Size(100, 22);
            this.txt_zahl2.TabIndex = 2;
            // 
            // cmd_show
            // 
            this.cmd_show.Location = new System.Drawing.Point(38, 236);
            this.cmd_show.Name = "cmd_show";
            this.cmd_show.Size = new System.Drawing.Size(89, 23);
            this.cmd_show.TabIndex = 3;
            this.cmd_show.Text = "&Überprüfen";
            this.cmd_show.UseVisualStyleBackColor = true;
            this.cmd_show.Click += new System.EventHandler(this.cmd_show_Click);
            // 
            // cmd_clear
            // 
            this.cmd_clear.Location = new System.Drawing.Point(158, 236);
            this.cmd_clear.Name = "cmd_clear";
            this.cmd_clear.Size = new System.Drawing.Size(88, 23);
            this.cmd_clear.TabIndex = 4;
            this.cmd_clear.Text = "&Löschen";
            this.cmd_clear.UseVisualStyleBackColor = true;
            this.cmd_clear.Click += new System.EventHandler(this.cmd_clear_Click);
            // 
            // cmd_end
            // 
            this.cmd_end.Location = new System.Drawing.Point(282, 236);
            this.cmd_end.Name = "cmd_end";
            this.cmd_end.Size = new System.Drawing.Size(102, 23);
            this.cmd_end.TabIndex = 5;
            this.cmd_end.Text = "&Beenden";
            this.cmd_end.UseVisualStyleBackColor = true;
            this.cmd_end.Click += new System.EventHandler(this.cmd_end_Click);
            // 
            // Zahlenvergleich
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(396, 322);
            this.Controls.Add(this.cmd_end);
            this.Controls.Add(this.cmd_clear);
            this.Controls.Add(this.cmd_show);
            this.Controls.Add(this.txt_zahl2);
            this.Controls.Add(this.txt_zahl1);
            this.Controls.Add(this.lbl_output);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Zahlenvergleich";
            this.Text = "Zahlenvergleich";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbl_output;
        private System.Windows.Forms.TextBox txt_zahl1;
        private System.Windows.Forms.TextBox txt_zahl2;
        private System.Windows.Forms.Button cmd_show;
        private System.Windows.Forms.Button cmd_clear;
        private System.Windows.Forms.Button cmd_end;
    }
}

